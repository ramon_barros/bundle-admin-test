<?php

Route::controller('admin::home');

Route::controller('admin::auth');

//Route::controller('admin::modulos.panel');

//Route::controller('admin::admin.panel');

Route::any('(:bundle)/modulos/(:any)', function($modulo='panel') {
    return Controller::call("admin::modulos.{$modulo}@index");
});

Route::any('(:bundle)/modulos/(:any)/(:any)', function($modulo='panel',$param='index') {
    return Controller::call("admin::modulos.{$modulo}@{$param}");
});

//admin/modulos/panel/edit/1
Route::any('(:bundle)/modulos/(:any)/(:any)/(:all)', function($modulo='panel',$param='index',$id=null) {
    $id = explode('/', $id);
    return Controller::call("admin::modulos.{$modulo}@{$param}",$id);
});